﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PomodoroApp
{
    class PomodoroApp
    {
        public PomodoroApp() : this(0, false, "Created by Adrian Lončar")
        {

        }
        public PomodoroApp(int vrijemerada, bool aktivan, string naziv)
        {
            this.vrijemeSekunde = vrijemerada;
            this.isActive = aktivan;
            this.appName = naziv;
        }
        public int vrijeme
        {
            get => vrijemeSekunde;
            set => vrijemeSekunde = value;

        }
        public bool Aktivnost
        {
            get => isActive;
            set => isActive = value;

        }
        public string Naziv
        {
            get => appName;
            set => appName = value;
        }
        private int vrijemeSekunde;
        private bool isActive;
        private string appName;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
