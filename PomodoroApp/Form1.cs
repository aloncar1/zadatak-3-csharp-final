﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PomodoroApp {
    public partial class frmMain : Form {
        int trenutniBrojSekundi;
        bool radPokrenut;

        private void azurirajPrikaz() {
            
           
            lblVrijeme.Text = string.Format("{0:D2}:{1:D2}", trenutniBrojSekundi / 60, trenutniBrojSekundi % 60);
            
            
            Text = string.Format("[{0:D2}:{1:D2}] PomodoroApp", trenutniBrojSekundi / 60, trenutniBrojSekundi % 60);

            if (radPokrenut) {
                lblStatus.BackColor = SystemColors.Control;
                lblStatus.Text = "RAD";
            } else {
                lblStatus.BackColor = Color.LimeGreen;
                lblStatus.Text = "ODMOR";
            }
        }

        private void azurirajTrenutanBrojSekundi() {
    
            if (radPokrenut) {
                trenutniBrojSekundi = int.Parse(tbRad.Text) * 60;
            } else {
                trenutniBrojSekundi = int.Parse(tbOdmor.Text) * 60;
            }
        }

        public frmMain() {
            InitializeComponent();
            
            radPokrenut = true;
            
            azurirajTrenutanBrojSekundi();
            azurirajPrikaz();
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            
            tmrOdbrojavanje.Enabled = !tmrOdbrojavanje.Enabled;
           
            if (tmrOdbrojavanje.Enabled) {
                tbRad.Enabled = false;
                tbOdmor.Enabled = false;
            } else {
                tbRad.Enabled = true;
                tbOdmor.Enabled = true;
            }
        }

        private void btnReset_Click(object sender, EventArgs e) {
        
            tmrOdbrojavanje.Enabled = false;
          
            tbRad.Enabled = true;
            tbOdmor.Enabled = true;

            
            radPokrenut = true;
            azurirajTrenutanBrojSekundi();
            azurirajPrikaz();
        }

        private void tmrOdbrojavanje_Tick(object sender, EventArgs e) {
            if (trenutniBrojSekundi == 0) {
                radPokrenut = !radPokrenut;
                azurirajTrenutanBrojSekundi();
            }
            trenutniBrojSekundi--;
            azurirajPrikaz();
        }
    }
}
